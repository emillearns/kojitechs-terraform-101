
# Create a VPC
resource "aws_vpc" "kojitechvpc" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "kojitechvpc"
  }
} 

# variable
# terraform
# resource
# provider
# outputblock

# private subnet
resource "aws_subnet" "private_sub" {
  vpc_id     = aws_vpc.kojitechvpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "is-east-1a"

  tags = {
    Name = "private_sub"
  }
}

resource "aws_subnet" "public_sub" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "public_sub"
  }
}

# 10.0.0.0/16
# 10.0.0.0/24, 10.0.2.0/24, 10.0.4.0/24 ...# public subnet
# 10.0.1.0/24, 10.0.3.0/24, 10.0.5.0/24 ...# private subnet

# Commands to push code to github/gitlab/bitbucket
# git status
#git add -A
#