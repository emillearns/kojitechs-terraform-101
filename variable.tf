
#create a variable: str
variable "vpc_cidr" {
    description = "vpc cidr"
    type = string
      default = "10.0.0.0/16"
}

variable "vpc_tag" {
    description = "vpc tag"
    type = string
    default = "kojitechvpc"
}
#types of variables
#"" = string
#bool = true/false
#number = 23
#list = [1,2,3,4,5,6]: list
#map ={}

variable "env" {
    type = map
    default = {
        prod = "1234543"
    }
  
}